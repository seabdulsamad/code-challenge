<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

/**
 * @property string cacheKey
 * @property int cacheTtl
 */
class HotelsHelper
{
    /**
     * Fetch the data from third party API using HTTP Client
     * @return array
     * @throws \Exception
     */
    public function getAllHotels(): array
    {
        $cacheKey = 'HotelsHelper.getAllHotels';
        $cacheTtl = 10;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $hotelsInfo = (new HttpClientHelper())->getData();
        Cache::add($cacheKey, $hotelsInfo, $cacheTtl);

        return Cache::get($cacheKey);
    }

    /**
     * @param $requestParams
     * @return $this
     */
    public function validate($requestParams)
    {
        $rules = [
            'sortBy'      =>  'in:name,price',
            'order'       =>  'in:desc,asc'
        ];

        $validator = Validator::make($requestParams, $rules);
        // Abort the validate pipe on error
        if ($validator->fails()) {
            $errors = $validator->messages()->toArray();
            abort(400,json_encode($errors));
        }

        return $this;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getResults(array $params) :array
    {
        $hotelsInfo = $this->getAllHotels();
        $hotelsInfo = $this->filterResults($hotelsInfo,$params);
        //Sort Data
        $sortBy     = $params['sortBy'] ?? 'name';
        $order      =  $params['order'] ?? 'asc';
        $hotelsInfo = $this->sortResults($sortBy, $order , $hotelsInfo);

        return $hotelsInfo;
    }

    /**
     * @param array $hotelsInfo
     * @param array $params
     * @return array
     */
    public function filterResults(array $hotelsInfo,array $params) :array
    {
        $response = [];
        foreach($hotelsInfo as $item){
            $flag = false;
            //Name Filter
            $name = $params['name'] ??'';
            if (!empty($name) && strpos($item['name'] ?? '', $name) !== false){
                $flag  = true;
            }
            //City or Destination Filter
            $city = $params['city'] ??'';
            if(!empty($city) && strpos($item['city'] ?? '', $city) !== false){
                $flag = true;
            }
            //Price Filter
            $price = $params['price'] ?? 0;
            $itemPrice = $item['price'] ?? 0;
            if(!empty($price) && strpos($price, ':') !== false){
                $price = str_replace('$','',$price);
                list($min,$max) = explode(':',$price);
                if ($itemPrice >= (float)$min && $itemPrice <= (float)$max){
                    $flag = true;
                }
            }
            //Date ot Availability Filter
            $date = $params['date'] ?? '';
            $itemAvailability = $item['availability'] ?? [];
            if(!empty($date) && strpos($date, ':') !== false){
                foreach($itemAvailability as $availability){
                    list($startDate,$endDate)= explode(':',$date);
                    //Parsing
                     $startDate = strtotime($startDate);
                     $endDate   = strtotime($endDate);
                     $availabilityFrom = $availability['from'] ?? '';
                     $availabilityTo = $availability['to'] ?? '';
                    if (strtotime($availabilityFrom) >= $startDate && strtotime($availabilityTo) <= $endDate) {
                        $flag = true;
                        break;
                    }
                }
            }

            if($flag){
                array_push($response, $item);
            }
        }

        return $response;
    }

    /**
     * Responsible for sorting of result
     *
     * @param $sortBy string
     * @param string $order
     * @param $dataToSort array
     *
     * @return array
     */
    public function sortResults(string $sortBy,string $order, array $dataToSort) :array
    {
        usort($dataToSort, function ($a, $b) use ($sortBy) {
            return strnatcmp($a[$sortBy], $b[$sortBy]);
        });

        if ($order == 'desc') {
            $dataToSort = array_reverse($dataToSort);
        }

        return $dataToSort;
    }

}