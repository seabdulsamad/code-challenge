<?php

namespace App\Helpers;

use App\Data\Message;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class HttpClientHelper {

    /**
     * @return array
     * @throws \Exception
     */
    public function getData() :array
    {
        $hotelUrl = env('HOTEL_CHANNEL_MANAGER_URL','');

        if (empty($hotelUrl)){
            throw new \Exception(Message::HOTEL_CHANNEL_MANAGER_URL_MISSING,400);
        }

        try {
            //Initiate the Http Guzzle request to third party server
            $http = new Client();
            $response = $http->request('GET', $hotelUrl);
            $data = $response->getBody()->getContents();
            $result = json_decode($data,true);
            return $result['hotels'] ?? [];

        } catch (\Exception $e) {
            //Error Logging
            Log::error($e->getMessage());
           throw new \Exception($e->getMessage(),400);
        }
    }
}