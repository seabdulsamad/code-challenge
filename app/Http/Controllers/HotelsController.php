<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use App\Helpers\HotelsHelper;
use Illuminate\Http\Request;

class HotelsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $data =  (new HotelsHelper())->validate($params)->getResults($params);
        return response()->json($data);
    }
}

