# Hotels REST Channel Manager

The Hotels REST Channel Manager is the REST API applied as a wrapper on the third party API to fetch the hotel data with availability, after some predefined interval and apply certain operations including searching, sorting and filtration to enhance the feature set.

## Getting Started

Clone the project on your machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to install the followings on your machine in order to setup the development environment on your  machine.

```
PHP >=7.1
```
Install the [composer](https://getcomposer.org/) The dependency manager on your machine to install the project dependencies.
```
PHP composer
```
Install the git on your machine
```
git
```
### Installing

Clone the project using the command below.

```
$ git clone https://bitbucket.org/seabdulsamad/code-challenge.git
```

Install project dependencies by running this command in the project's root directory:

```
$ composer install
```
If you change structure, paths, namespaces, etc., make sure you run the [autoload generator](https://getcomposer.org/doc/03-cli.md#dump-autoload):
```sh
$ composer dump-autoload
```

## Running the tests

To run the tests:
```
$ php vendor/phpunit/phpunit/phpunit
```

## Built With

* [Lumen](https://lumen.laravel.com/docs/5.6) - The stunningly fast micro-framework by Laravel.
* [Composer](https://getcomposer.org/doc/) - Dependency Management
* [Guzzle](http://docs.guzzlephp.org/en/stable/) - HTTP client to communicate with third party API

## Assumptions

* Assuming that the price received from the third-party API is already in USD so we don't need any currency rate(conversion) API
* Assuming that the we'll fetch the data from the API after every 1 hour and will keep the data in file based cache in our application.

## Endpoint

* **URL**
```
http://site-url.com/hotels/
```
* **Method**
```
GET
```
* **Request Filters**
    Below are the supported filters

    | Name | Description |
    | --- | --- |
    | `name` | Can be free text |
    | `city` | Can be free text |
    | `price` | Must have the format `$80:$100` can be used with our without `$` sign. |
    | `date` | Must have the format `d-m-Y:d-m-Y` |
    | `sortBy` | Can be only `name` or `price` |
    | `order` | Can be only `asc` or `desc` |

## Authors

* **Abdul Samad** - se.abdulsamad@gmail.com

## License

This project is licensed under the MIT License [Abdul Samad](http://linkedin.com/in/abdul-samad-993b8450)