<?php

use App\Helpers\HotelsHelper;

class HotelTest extends TestCase
{
    /**
     * Test can throw exception with wrong name filter
     *
     * @dataProvider dataForWrongNameFilter
     */
    public function testCanThrowExceptionWithWrongNameFilter($url)
    {
        $response = $this->json('GET', $url);

        $this->assertResponseStatus(400);
    }

    public function dataForWrongNameFilter()
    {
        return [
            ['/hotels?order=desc&sortBy=test'],
            ['/hotels?order=desc&sortBy=abc'],
            ['/hotels?order=desc&sortBy=xyz'],
            ['/hotels?order=desc&sortBy=foo'],
        ];
    }

    /**
     * Test can throw exception with wrong name filter
     *
     * @dataProvider dataForWrongOrderFilter
     */
    public function testCanThrowExceptionWithWrongOrderFilter($url)
    {
        $response = $this->json('GET', $url);

        $this->assertResponseStatus(400);
    }

    public function dataForWrongOrderFilter()
    {
        return [
            ['/hotels?order=test&sortBy=price'],
            ['/hotels?order=abc&sortBy=price'],
            ['/hotels?order=xyz&sortBy=price'],
            ['/hotels?order=foo&sortBy=price'],
        ];
    }

    /**
     * Test can throw exception with wrong name filter
     *
     * @dataProvider dataForCorrectOrderFilter
     */
    public function testSuccessfullResponseWithCorrectFilter($url)
    {
        $response = $this->json('GET', $url);

        $this->assertResponseOk();
    }

    public function dataForCorrectOrderFilter()
    {
        return [
            ['/hotels?order=asc&sortBy=name'],
            ['/hotels?order=asc&sortBy=price'],
            ['/hotels?order=desc&sortBy=name'],
            ['/hotels?order=desc&sortBy=price'],
        ];
    }

    /**
     * @param string $sortBy
     * @param string $order
     * @param array  $data
     * @param array  $expected
     *
     * @dataProvider dataForHotelSort
     *
     * @group        sort-hotels
     */
    public function testCanSortHotels(string $sortBy, string $order, array $data, array $expected)
    {
        $response = (new HotelsHelper())->sortResults($sortBy, $order, $data);

        $this->assertEquals($expected, $response);
    }

    public function dataForHotelSort()
    {
        return [
            ['name', 'asc', [
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
            ], [
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
            ]],
            ['name', 'desc', [
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
            ], [
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
            ]],
            ['price', 'asc', [
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
            ], [
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
            ]],
            ['price', 'desc', [
                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],
                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],
                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
            ], [
                [
                    "name"  => "Novotel Hotel",
                    "price" => 111,
                ],

                [
                    "name"  => "Golden Tulip",
                    "price" => 109.6,
                ],

                [
                    "name"  => "Media One Hotel",
                    "price" => 102.2,
                ],

                [
                    "name"  => "Le Meridien",
                    "price" => 89.6,
                ],

                [
                    "name"  => "Rotana Hotel",
                    "price" => 80.6,
                ],
                [
                    "name"  => "Concorde Hotel",
                    "price" => 79.4,
                ],
            ]],
        ];
    }
}
